package fr.sma.entretien.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import fr.sma.entretien.beans.Apple;
import org.apache.commons.io.IOUtils;
import org.springframework.core.io.Resource;
import java.io.IOException;
import java.io.InputStream;
import java.io.StringWriter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;


public class AppleRestController {

    ObjectMapper mapper = new ObjectMapper();
    List<Apple> getApplesFromJson() {

        List<Apple> response = new ArrayList<Apple>();
        return response;
    }

    private List<Apple> convertStreamToApples(InputStream stream) {
        List<Apple> liste = new ArrayList<>();
        StringWriter writer = new StringWriter();
        try {
            IOUtils.copy(stream, writer, "UTF-8");
            String json = writer.toString();
            liste = Arrays.asList(mapper.readValue(json, Apple[].class));
        } catch (IOException e) {
            e.printStackTrace();
        }
        return liste;
    }

}
